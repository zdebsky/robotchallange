﻿using Robot.Common;
using System;
using System.Collections.Generic;

namespace PetroZdebsky.RobotChallange
{
    public class PetroZdebskyAlgorithm : IRobotAlgorithm
    {
        public PetroZdebskyAlgorithm()
        {
            Logger.OnLogRound += Logger_OnLogRound;
        }

        private void Logger_OnLogRound(object sender, LogRoundEventArgs e)
        {
            RoundCount++;
        }

        public int RoundCount { get; set; }
        public string Author
        {
            get
            {
                return "Petro Zebsky";
            }
        }

        public string Description
        {
            get
            {
                return "My robot algorithm";

            }
        }

        // - 50
        public Position FindNearestFreeStation(Robot.Common.Robot movingRobot, Map map, IList<Robot.Common.Robot> robots)
        {
            EnergyStation nearest = null;
            int minDistance = int.MaxValue;
            foreach (var station in map.Stations)
            {
                if (IsStationFree(station, movingRobot, robots))
                {
                    int d = DistanceHelper.FindDistance(station.Position, movingRobot.Position);

                    if (d < minDistance)
                    {
                        minDistance = d;
                        nearest = station;
                    }
                }
            }

            return nearest == null ? null : nearest.Position;
        }

        public bool IsStationFree(EnergyStation station, Robot.Common.Robot movingRobot, IList<Robot.Common.Robot> robots)
        {
            return IsCellFree(station.Position, movingRobot, robots);
        }


        public bool IsCellFree(Position cell, Robot.Common.Robot movingRobot, IList<Robot.Common.Robot> robots)
        {
            foreach (var robot in robots)
            {
                if (robot != movingRobot)
                {
                    if (robot.Position == cell)
                        return false;
                }
            }
            return true;
        }

        public RobotCommand DoStep(IList<Robot.Common.Robot> robots, int robotToMoveIndex, Map map)
        {

            if ((robots[robotToMoveIndex].Energy > 400) && (robots.Count < map.Stations.Count))
            {
                return new CreateNewRobotCommand();
            }
            var nearestStations = new List<EnergyStation>();
            var array = new List<Robot.Common.Robot>();
            foreach (var robot in robots)
            {
                if (robot.Owner.Name == Author)
                {
                    array.Add(robot);
                }
            }
            bool flag;
            List<EnergyStation> strangerStations = new List<EnergyStation>();
            foreach (var station in map.Stations)
            {
                flag = true;
                foreach (var robot in array)
                {
                    if (robot != robots[robotToMoveIndex] && station.Position == robot.Position)
                    {
                        flag = false;
                    }
                }
                if (flag)
                {
                    strangerStations.Add(station);
                }
            }


            Position newPosition = new Position();
            int min = int.MaxValue;
            foreach (var station in strangerStations)
            {
                if (DistanceHelper.FindDistance(robots[robotToMoveIndex].Position, station.Position) < min)
                {
                    min = DistanceHelper.FindDistance(robots[robotToMoveIndex].Position, station.Position);
                    newPosition = station.Position;
                }
            }

            if ((RoundCount == 10 || RoundCount == 25 || RoundCount == 35 || RoundCount == 20 || RoundCount == 30) && robots[robotToMoveIndex].Energy > 400)
            {
                if (FindNearestFreeStation(robots[robotToMoveIndex], map, robots) != null)
                {
                    foreach (var station in map.Stations)
                    {
                        if (robots[robotToMoveIndex].Position == station.Position)
                        {
                            CreateNewRobotCommand command = new CreateNewRobotCommand();
                            command.NewRobotEnergy = robots[robotToMoveIndex].Energy - 300;
                            return command;
                        }
                    }
                }
            }
            if (newPosition == robots[robotToMoveIndex].Position)
                return new CollectEnergyCommand();
            int dx, dy;
            int distance = DistanceHelper.FindDistance(robots[robotToMoveIndex].Position, newPosition);
            distance = DistanceHelper.FindDistance(robots[robotToMoveIndex].Position, newPosition);
            if (robots[robotToMoveIndex].Energy < distance && distance < 100)
            {
                dx = Math.Min(Math.Abs(robots[robotToMoveIndex].Position.X - newPosition.X), 7) * Math.Sign(newPosition.X - robots[robotToMoveIndex].Position.X);
                dy = Math.Min(Math.Abs(robots[robotToMoveIndex].Position.Y - newPosition.Y), 7) * Math.Sign(newPosition.Y - robots[robotToMoveIndex].Position.Y);
                newPosition = new Position(robots[robotToMoveIndex].Position.X + dx, robots[robotToMoveIndex].Position.Y + dy);
            }
            if (robots[robotToMoveIndex].Energy < distance && distance > 100 && distance < 200)
            {
                dx = Math.Min(Math.Abs(robots[robotToMoveIndex].Position.X - newPosition.X), 10) * Math.Sign(newPosition.X - robots[robotToMoveIndex].Position.X);
                dy = Math.Min(Math.Abs(robots[robotToMoveIndex].Position.Y - newPosition.Y), 10) * Math.Sign(newPosition.Y - robots[robotToMoveIndex].Position.Y);
                newPosition = new Position(robots[robotToMoveIndex].Position.X + dx, robots[robotToMoveIndex].Position.Y + dy);
            }
            distance = DistanceHelper.FindDistance(robots[robotToMoveIndex].Position, newPosition);
            if (robots[robotToMoveIndex].Energy - 50 < distance)
            {
                dx = Math.Min(Math.Abs(robots[robotToMoveIndex].Position.X - newPosition.X), 1) * Math.Sign(newPosition.X - robots[robotToMoveIndex].Position.X);
                dy = Math.Min(Math.Abs(robots[robotToMoveIndex].Position.Y - newPosition.Y), 1) * Math.Sign(newPosition.Y - robots[robotToMoveIndex].Position.Y);
                newPosition = new Position(robots[robotToMoveIndex].Position.X + dx, robots[robotToMoveIndex].Position.Y + dy);
            }
            return new MoveCommand() { NewPosition = newPosition };
        }

    }

    public class DistanceHelper
    {
        public static int FindDistance(Position a, Position b)
        {
            return (int)(Math.Pow(a.X - b.X, 2) + Math.Pow(a.Y - b.Y, 2));
        }
    }

}
