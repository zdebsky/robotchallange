﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using Robot.Common;

namespace PetroZdebsky.RobotChallange.Test {
    [TestClass]
    public class TestAlgorithm {
        [TestMethod]
        public void TestAttackChooseWhileCollectring () {
            var algorithm = new PetroZdebskyAlgorithm();
            var map = new Map();
            Owner meOwner = new Owner();
            Owner enemyOwner = new Owner();
            meOwner.Name = "Petro Zdebsky";
            enemyOwner.Name = "Oleg Zdebsky";
            var stationPosition = new Position(1, 1);
            map.Stations.Add(new EnergyStation() { Energy = 200, Position = stationPosition, RecoveryRate = 2 });
            var robots = new List<Robot.Common.Robot>() { new Robot.Common.Robot() { Energy = 1000, Position = new Position(1, 1), Owner = meOwner }, new Robot.Common.Robot() { Energy = 10000, Position = new Position(3, 3), Owner = enemyOwner } };
            var command = algorithm.DoStep(robots, 0, map);

            Assert.IsTrue(command is MoveCommand);
            Assert.AreEqual(((MoveCommand) command).NewPosition, robots[1].Position);
        }

        [TestMethod]
        public void TestCreateNewCommand () {
            var algorithm = new PetroZdebskyAlgorithm();
            var map = new Map();
            var stationPosition = new Position(1, 1);
            map.Stations.Add(new EnergyStation() { Energy = 100, Position = stationPosition, RecoveryRate = 2 });
            var robots = new List<Robot.Common.Robot>() { new Robot.Common.Robot() { Energy = 1000, Position = new Position(99, 99) } };
            var command = algorithm.DoStep(robots, 0, map);

            Assert.IsTrue(command is CreateNewRobotCommand);
        }

        [TestMethod]
        public void TestAttack () {
            var algorithm = new PetroZdebskyAlgorithm();
            var map = new Map();
            Owner meOwner = new Owner();
            Owner enemyOwner = new Owner();
            meOwner.Name = "Petro Zdebsky";
            enemyOwner.Name = "Oleg Zdebsky";
            var stationPosition = new Position(1, 1);
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = stationPosition, RecoveryRate = 2 });
            var robots = new List<Robot.Common.Robot>() { new Robot.Common.Robot() { Energy = 500, Position = new Position(80, 80), Owner = meOwner }, new Robot.Common.Robot() { Energy = 3000, Position = new Position(81, 81), Owner = enemyOwner } };
            var command = algorithm.DoStep(robots, 0, map);

            Assert.AreEqual(robots[1].Energy, 2850);
        }

        [TestMethod]
        public void TestStationChoosing () {
            var algorithm = new PetroZdebskyAlgorithm();
            var map = new Map();
            var stationPosition1 = new Position(55, 50);
            var stationPosition2 = new Position(50, 50);
            map.Stations.Add(new EnergyStation() { Energy = 10000, Position = stationPosition1, RecoveryRate = 2 });
            map.Stations.Add(new EnergyStation() { Energy = 500, Position = stationPosition2, RecoveryRate = 2 });
            var robots = new List<Robot.Common.Robot>() { new Robot.Common.Robot() { Energy = 500, Position = new Position(51, 50) } };
            var command = algorithm.DoStep(robots, 0, map);

            Assert.AreEqual(robots[0].Position, stationPosition1);
        }

        [TestMethod]
        public void TestFindNearestFreeStation () {
            var algorithm = new PetroZdebskyAlgorithm();
            var map = new Map();
            var stationPosition1 = new Position(10, 10);
            var stationPosition2 = new Position(55, 55);
            map.Stations.Add(new EnergyStation() { Energy = 500, Position = stationPosition1, RecoveryRate = 2 });
            map.Stations.Add(new EnergyStation() { Energy = 500, Position = stationPosition2, RecoveryRate = 2 });
            var robots = new List<Robot.Common.Robot>() { new Robot.Common.Robot() { Energy = 500, Position = new Position(60, 55) } };

            var nearestStationPosition = algorithm.FindNearestFreeStation(robots[0], map, robots);

            Assert.AreEqual(nearestStationPosition, stationPosition2);
        }

        [TestMethod]
        public void TestCreateNewWhenCollecting () {
            var algorithm = new PetroZdebskyAlgorithm();
            var map = new Map();
            var stationPosition1 = new Position(10, 10);
            map.Stations.Add(new EnergyStation() { Energy = 500, Position = stationPosition1, RecoveryRate = 2 });
            var robots = new List<Robot.Common.Robot>() { new Robot.Common.Robot() { Energy = 1000, Position = new Position(10, 10) } };
            var command = algorithm.DoStep(robots, 0, map);

            if ( robots.Count <= map.Stations.Count ) {
                Assert.IsTrue(command is CreateNewRobotCommand);
            } else {
                Assert.IsTrue(command is CollectEnergyCommand);
            }
        }

        [TestMethod]
        public void TestCreateNewWhenEnergyNotEnough () {
            var algorithm = new PetroZdebskyAlgorithm();
            var map = new Map();
            map.Stations.Add(new EnergyStation() { Energy = 500, Position = new Position(10, 10), RecoveryRate = 2 });
            map.Stations.Add(new EnergyStation() { Energy = 500, Position = new Position(9, 10), RecoveryRate = 3 });
            var robots = new List<Robot.Common.Robot>() { new Robot.Common.Robot() { Energy = 125, Position = new Position(15, 15) } };
            var command = algorithm.DoStep(robots, 0, map);

            Assert.IsFalse(command is CreateNewRobotCommand);
        }

        [TestMethod]
        public void TestCreateNewWhenEnergyEnough () {
            var algorithm = new PetroZdebskyAlgorithm();
            var map = new Map();
            map.Stations.Add(new EnergyStation() { Energy = 500, Position = new Position(10, 10), RecoveryRate = 2 });
            map.Stations.Add(new EnergyStation() { Energy = 500, Position = new Position(9, 10), RecoveryRate = 3 });
            var robots = new List<Robot.Common.Robot>() { new Robot.Common.Robot() { Energy = 1000, Position = new Position(15, 15) } };
            var command = algorithm.DoStep(robots, 0, map);

            Assert.IsTrue(command is CreateNewRobotCommand);
        }

        [TestMethod]
        public void TestSeparatingSteps () {
            var algorithm = new PetroZdebskyAlgorithm();
            var map = new Map();
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = new Position(1, 1), RecoveryRate = 2 });
            var robots = new List<Robot.Common.Robot>() { new Robot.Common.Robot() { Energy = 100, Position = new Position(99, 99) } };
            Position oldPosition = robots[0].Position;
            var command = algorithm.DoStep(robots, 0, map);

            Assert.IsTrue(command is MoveCommand);
            Assert.AreNotEqual(robots[0].Position, map.Stations[0].Position);
            Assert.AreNotEqual(robots[0].Position, oldPosition);
        }

        [TestMethod]
        public void TestNotAttackOwn () {
            var algorithm = new PetroZdebskyAlgorithm();
            var map = new Map();
            Owner owner = new Owner();
            owner.Name = "Petro Zdebsky";
            var robots = new List<Robot.Common.Robot>() { new Robot.Common.Robot() { Energy = 100, Position = new Position(50, 50), Owner = owner }, new Robot.Common.Robot() { Energy = 100, Position = new Position(50, 51), Owner = owner } };
            Position oldPosition = robots[1].Position;
            var command = algorithm.DoStep(robots, 0, map);

            Assert.AreNotEqual(robots[0].Position, oldPosition);
        }
    }
}
